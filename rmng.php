<?php
/**
 * @package Respano Manager
 * @version 0.4
 */
/*
Plugin Name: Respano Manager
Description: This is a Wordpress plugin that enables your blog to serve as a restaurant webpage by using the Respano API.
Author: Florian Ritzel
Version: 0.4
Author URI: http://ritzel.me/
*/

$dir = plugin_dir_path( __FILE__ );
require_once $dir.'options.php';
require_once $dir.'dataprovider.php';

function contactinfo_add_shortcode($atts) {
	$dataProvider = new DataProvider();
	$contactData = NULL;
	$queryString = 'body.restaurant.contactInformation'; // access JSON data JS style
	$contactData =  $dataProvider->getData($queryString);	
	if(!is_null($contactData)){
		return
			'<iframe width="100%" height="370" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='.$contactData['geographicalPosition']['latitude'].','.$contactData['geographicalPosition']['longitude'].'&hl=es;z=14&amp;output=embed"></iframe>'.
			'<dl>'.
			'<dt>Telefon: </dt><dd>'.$contactData['phoneNumbers'][0]['value'].'</dd>'.
			'<dt>Stra&szlig;e: </dt><dd>'.$contactData['address']['street'].'</dd>'.
			'<dt>Postleitzahl: </dt><dd>'.$contactData['address']['zip'].'</dd>'.
			'<dt>Stadt: </dt><dd>'.$contactData['address']['city'].'</dd>'.			
			'</dl>'
		;
	} else {
		return '<p>Zu diesem Zeitpunkt sind noch keine Kontaktdaten vorhanden.</p>';
	}

}

function panorama_add_shortcode($atts) {
	$panoramaUrl = get_option('rmng_panorama_url');
	$rmngBid = get_option('rmng_bid');

	if($rmngBid == ''){
		return 'Please specify a BID in the <a href="wp-admin/options-general.php?page=rmng-settings">Respano Manager</a>.';
	}

	if (substr($panoramaUrl, -1) !== '/') {
	    $panoramaUrl = $panoramaUrl."/";
	}

	extract( shortcode_atts(
		array(
			'mode' => '',
			'width' => '100%',
			'height' => '500px'
		), $atts )
	);

	$panoramaStyle = 'style="width: '.$width.'; height: '.$height.'; border: none;';
	$panoramaMode = $mode == '' ? '' : '&mode='.$mode;
	$panoramaTarget = $panoramaUrl.'?bid='.$rmngBid.$panoramaMode;

	return '<iframe scrolling="no" marginheight="0" marginwidth="0" src="'.$panoramaTarget.'" '.$panoramaStyle.'"></iframe>';
}

function rmng_install() {
	add_option("rmng_user", "");
	add_option("rmng_pass", "");
	add_option("rmng_bid", "");
	add_option("rmng_client_id", "");
	add_option("rmng_client_secret", "");
	add_option("rmng_rest_url", "http://test.respano.com/rest/restaurant/published/");
	add_option("rmng_panorama_url", "http://test.respano.com/view/");
	add_option("rmng_bid", "");
}

add_shortcode('rmng_panorama', 'panorama_add_shortcode');
add_shortcode('rmng_contactinfo', 'contactinfo_add_shortcode');

register_activation_hook(__FILE__, 'rmng_install');

?>