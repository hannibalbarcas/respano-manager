<?php

class DataProvider {	
	
	function __construct() {

		$datetime = new DateTime();
	    $this->providerData =	array(
			'url' => get_option('rmng_rest_url').get_option('rmng_bid'),
			'user' => get_option('rmng_user'),
			'pass' => get_option('rmng_pass'),
			'clientid' => get_option('rmng_client_id'),
			'clientsecret' => get_option('rmng_client_secret'),
			'date' => $datetime->format('c')
		);		
	}

    // Build the signature to access the Respano API
    function getSignature($clientid, $clientSecret, $username, $password, $date){
		$passwordSha = hash('sha256', $password.$username.strrev($username));
		$secrethash = $passwordSha.$clientSecret;
		$hmac =  $passwordSha."\n".$clientSecret."\n".$date;
		$hmacSha = hash_hmac('sha256', $hmac, $secrethash);
		$hmacHash = utf8_encode($hmacSha);
		$hmacBase = base64_encode($hmacHash);
    	return $hmacBase;
    }

    // Get the data via Respano API in JSON
    function getJSON(){
  	
    	$signature = $this->getSignature($this->providerData['clientid'], $this->providerData['clientsecret'], $this->providerData['user'], $this->providerData['pass'], $this->providerData['date']);
    	if(is_null($signature) || $signature == ""){
    		return NULL;
    	}
		$opts = array(
		  'http'=>array(
		    'method'=>"GET",
		    'header'=> 
		    	"X-Respano-Date: ".$this->providerData['date']."\r\n" .
		    	"Authorization: RESPANO ".$this->providerData['clientid'].":".$this->providerData['user'].":".$signature."\r\n".
				"Content-Type: application/json\r\n"
		  )
		);
		$context = stream_context_create($opts);
		$file = file_get_contents($this->providerData["url"], false, $context);
		$json = $file == "" ? json_decode("{}", true) : json_decode($file, true);
		return $json;
    }

    // Access JSON blob via fields in a string, seperated by '.'
    function getJSONField($data, $attr){
    	$fields = preg_split('/\./', $attr);
    	$currentSet = $data;
    	if(!is_null($fields)){
    		for($i = 0; $i<sizeof($fields); $i++){
    			$currentSet = $currentSet[$fields[$i]];
    		}
    	}
    	var_dump($currentSet);
    	return $currentSet;
    }

    // Get the data as a PHP array representation of the JSON
    public function getData($attr) {    	
        $data = $this->getJSON();
        $data = $this->getJSONField($data, $attr);
        if(!is_null($data)){
        	return $data;
        } else {
        	return NULL;
        }
    }
}

?>