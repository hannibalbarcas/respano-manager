<?php

if ( is_admin() ){
  add_action( 'admin_menu', 'add_rmng_admin_menu' );
  add_action( 'admin_init', 'register_rmng_settings' );
} else {
  // non-admin enqueues, actions, and filters
}

function add_rmng_admin_menu() {

	add_options_page( 'Respano Manager', 'Respano Manager', 'manage_options', 'rmng-settings', 'create_admin_page' );

}

function register_rmng_settings() { // whitelist options
	register_setting( 'rmng-settings-group', 'rmng_user' );
	register_setting( 'rmng-settings-group', 'rmng_pass' );
  	register_setting( 'rmng-settings-group', 'rmng_panorama_url' );
  	register_setting( 'rmng-settings-group', 'rmng_bid' );
  	register_setting( 'rmng-settings-group', 'rmng_panorama_mode' );
  	register_setting( 'rmng-settings-group', 'rmng_client_id' );
  	register_setting( 'rmng-settings-group', 'rmng_client_secret' );

	add_settings_section( 'section-one', 'User', 'section_one_callback', 'rmng' );	
	add_settings_field( 'rmng_user', 'User', 'user_callback', 'rmng', 'section-one' );
	add_settings_field( 'rmng_pass', 'Password', 'password_callback', 'rmng', 'section-one' );
	add_settings_field( 'rmng_bid', 'BID', 'bid_callback', 'rmng', 'section-one' );

	add_settings_section( 'section-two', 'API', 'section_one_callback', 'rmng' );
	add_settings_field( 'rmng_client_id', 'Client ID', 'client_id_callback', 'rmng', 'section-two' );
	add_settings_field( 'rmng_client_secret', 'Client secret', 'client_secret_callback', 'rmng', 'section-two' );
	add_settings_field( 'rmng_rest_url', 'Rest URL', 'rest_url_callback', 'rmng', 'section-two' );
	add_settings_field( 'rmng_panorama_url', 'Panorama URL', 'panorama_url_callback', 'rmng', 'section-two' );
}

function section_one_callback() {
    echo 'User settings:';
}


function section_two_callback() {
    echo 'API settings:';
}

function user_callback() {
    $setting = esc_attr( get_option( 'rmng_user' ) );
    echo "<input type='text' name='rmng_user' value='$setting' placeholder='username' />";
}

function password_callback() {
    $setting = esc_attr( get_option( 'rmng_pass' ) );

    echo "<input type='password' name='rmng_pass' value='$setting' placeholder='very secret' />";
}

function bid_callback() {
    $setting = esc_attr( get_option( 'rmng_bid' ) );
    echo "<input type='text' name='rmng_bid' value='$setting' placeholder='bid' />";
}


function client_id_callback() {
    $setting = esc_attr( get_option( 'rmng_client_id' ) );
    echo "<input type='text' name='rmng_client_id' value='$setting' placeholder='id' />";
}

function client_secret_callback() {
    $setting = esc_attr( get_option( 'rmng_client_secret' ) );
    echo "<input type='password' name='rmng_client_secret' value='$setting' placeholder='very secret' />";
}

function rest_url_callback() {
    $setting = esc_attr( get_option( 'rmng_rest_url' ) );
    echo "<input type='url' name='rmng_panorama_url' value='$setting' pattern='https?://.+' placeholder='http://url/' />";
}

function panorama_url_callback() {
    $setting = esc_attr( get_option( 'rmng_panorama_url' ) );
    echo "<input type='url' name='rmng_panorama_url' value='$setting' pattern='https?://.+' placeholder='http://url/' />";
}

function create_admin_page() {

	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	echo '<div class="wrap">';
	echo '<?php screen_icon(); ?>';
	echo '<h2>Respano Manager</h2>';
	echo '<h3>Shortcodes</h3>';
	echo '<p>Use <strong>[rmng_panorama]</strong> as a shortcode in posts to display a panorama. Change the default values in the Panoramix settings. You can use attributes in the shortcode like so: <strong>[rmng_panorama mode="lookonly" width="500px" height="200px"]</strong>.<p>Use <strong>[rmng_contactinfo]</strong> to show contact info. A bunch of stuff will show up, work in progress.</p>';
	echo '<form method="post" action="options.php">';

	settings_fields( 'rmng-settings-group' );
	do_settings_sections( 'rmng' );
	submit_button();

	echo '</form>';
	echo '</div>';

}
?>